import React, { Component } from 'react'

export default class TableComponent extends Component {
    render() {
        return (
            <tr>
            <td>{this.props.todoInfo.id}</td>
            <td>{this.props.todoInfo.title}</td>
            <td>{this.props.todoInfo.price}</td>
            <td>{this.props.todoInfo.description}</td>
            <td>{this.props.todoInfo.category}</td>
            <td style={{border: '1px solid black'}}> <img src ={this.props.todoInfo.image} width="50"/></td>
    </tr>
    
);
}
}

