import React, { Component } from 'react'
import axios from 'axios'
import TodoList from './TodoList'

export default class FetchHandler extends Component {

    state={
        todos:[]
    }

    componentDidMount(){
        this.getDataFromAPI()
    }

    getDataFromAPI() {
        let url= "https://fakestoreapi.com/products";
        // Make a request for a user with a given ID
        axios.get(url)
            .then(response => {
            this.setState({
                todos: response.data
            })
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .then(function () {
                // always executed
            });
    }

    render() {
        return (
            <div>
                <h1>Fetch API</h1>
                <TodoList data={this.state.todos}/>
            </div>
        )
    }
}
